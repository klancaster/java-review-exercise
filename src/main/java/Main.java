import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {
        System.out.println("Java Review");
    }

    /**
     * Print out all the odd numbers in the range [low,high] on a single
     * line, separated by commas
     */
    public static void printOddNumbersInRange(int low, int high) {

    }

    /**
     * Return the minimum value from the parameters i,j,k,l
     * @param i
     * @param j
     * @param k
     * @param l
     * @return
     */
    public static int getMinimumValue(int i, int j, int k, int l) {
        return 0;
    }

    /**
     * Return the minimum value in the array
     * @param testArray
     * @return
     */
    public static int minOfArray(int[] testArray) {
        return 0;
    }

    /**
     * Concatentate array1 and array2
     * @param array1
     * @param array2
     * @return
     */
    public static int[] concatArrays(int[] array1, int[] array2) {
        return new int[0]; // placeholder so that code will compile
    }

    /**
     * Replace the placeholders in the baseString with the supplied values
     * @param baseString
     * @param value
     * @param type
     * @return
     */
    public static String subValuesInString(String baseString, int value, String type) {
        return null;
    }

    /**
     * Iterate over the list to generate a list of just the names, separated by commas
     * @param people
     * @return
     */
    public static String printCollection(List<Person> people) {
        return null; 
    }
}
