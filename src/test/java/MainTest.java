import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class MainTest {

    // This approach to testing System.out is taken from with only minor modifications
    // https://stackoverflow.com/questions/1119385/junit-test-for-system-out-println
    private final ByteArrayOutputStream outputContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errorContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outputContent));
        System.setErr(new PrintStream(errorContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }


    @Test
    public void testPrintCapture(){
        System.out.print("A test");
        assertEquals("A test", outputContent.toString());

    }

    @Test
    public void testPrintingOddNumbers(){
        Main.printOddNumbersInRange(1,9);
        String expected = "1,3,5,7,9";
        assertEquals(expected, outputContent.toString());
    }

    @Test
    public void testConditionalLogic(){
        assertEquals(1,Main.getMinimumValue(1,5,3,7));
    }

    @Test
    public void testConditionalLogic2(){
        assertEquals(-1,Main.getMinimumValue(7,11,-1,20));
    }

    @Test
    public void testFindMinimumInArray(){
        int[] testArray = {1,5,3,9,20};
        assertEquals(1, Main.minOfArray(testArray));
    }

    @Test
    public void testFindMinimumInArray2(){
        int[] testArray = {8,5,3,9,20};
        assertEquals(3, Main.minOfArray(testArray));
    }

    @Test
    public void testConcatenateArrays(){
        int[] array1 = {1,2,3};
        int[] array2 = {4,5,6};
        int[] expected = {1,2,3,4,5,6};
        assertTrue(testEquality(expected,Main.concatArrays(array1,array2)));
    }

    /**
     * Test to see of two int arrays are identical
     * @param expected
     * @param returned
     * @return
     */
    private boolean testEquality(int[] expected, int[] returned){
        for (int i = 0; i < expected.length; i++) {
            if(expected[i] != returned[i]){
                return false;
            }
        }
        return true;
    }

    @Test
    public void testConcatenateArrays2(){
        int[] array1 = {1,2,7};
        int[] array2 = {4,5,6};
        assertTrue(testEquality(new int[]{1,2,7,4,5,6},Main.concatArrays(array1,array2)));
    }

    @Test
    public void testSubstitutingValues(){
        String baseString = "The value is %d and its type is %s";
        int value = 1;
        var type = "integer";
        assertEquals("The value is 1 and its type is integer", Main.subValuesInString(baseString,value,type));
    }

    @Test
    public void testSubstitutingValues2(){
        String baseString = "The value for type %s is %d";
        int value = 10;
        var type = "Integer";
        assertEquals("The value for type Integer is 10", Main.subValuesInString(baseString,value,type));
    }

    @Test
    public void testPrintingAList(){
        List<Person> people =Arrays.asList(new Person("John"), new Person("Harry"), new Person("Alfred"));
        assertEquals("John,Harry,Alfred", Main.printCollection(people));
    }


}
